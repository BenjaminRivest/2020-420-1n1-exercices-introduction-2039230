package ca.cegepdrummond;

import java.util.Scanner;

public class Serie1_Introduction {

    /**
     * Comme dans tout bon cours d'introduction, il est d'usage de faire un "Hello world!"
     * afin de vous présenter le langage.
     *
     * Afin de nous assurer que votre environnement de programmation veuillez essayer d'exécuter cette fonction
     * en indiquant que vous désirez exécuter l'exercice #1
     * Vous pouvez ensuite exécuter le test
     */
    public void HelloWorld() {
        System.out.println("Hello World!");
    }

    /*
     * complétez cette fonction afin d'afficher "Bonjour"
     */
    public void print1() {

            System.out.println("Bonjour");

    }

    /*
     * complétez cette fonction afin d'afficher "Bonjour"
     */
    public void print2() {

        System.out.println("Bonjour");

    }

    /*
     * complétez cette fonction afin d'afficher "Bonjour"
     */
    public void print3() {

        System.out.println("Bonjour");

    }





}
